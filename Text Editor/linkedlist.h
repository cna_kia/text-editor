#ifndef LINKEDLIST_H
#define LINKEDLIST_H
template<class t>
struct node {
    t  data;
    node *next;
    node *prev;
};
template<class t>
class LinkedList
{
public:

    void insert(node<t> *n, t &x);
    void Delete(const t);
    void add(t data);
    //void traverse();
    node<t>* getnode(int n);
    node<t> *first;
    LinkedList();
    ~LinkedList();
    void DeleteNode(node<t> *n);
    t head();
    t tail();
    int length();
    void display();
    const t stringreturn();
    t JosephUs(int i, int j);
};
#endif // LINKEDLIST_H
