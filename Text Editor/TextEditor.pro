#-------------------------------------------------
#
# Project created by QtCreator 2015-11-25T20:18:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TextEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    linkedlist.cpp

HEADERS  += mainwindow.h \
    linkedlist.h

FORMS    += mainwindow.ui

RESOURCES += \
    image.qrc
