#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <linkedlist.h>
#include <QTextDocument>
#include <QTextBlock>
#include <QLinkedList>
#include <QPushButton>
#include <QLineEdit>
#include <QTextCursor>
#include <QUndoCommand>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    int italic=0;
    int bold=0;
    explicit MainWindow(QWidget *parent = 0);
    QFileDialog* a;
    QMessageBox *msg;
    QLabel *w;
    QDockWidget* Find;
    QDockWidget* Delete;
    QDockWidget* Insert;
    QPushButton* del;
    QPushButton* Ins;
    QTextCharFormat *format;
    QTextCharFormat tempformat;
    QTextCharFormat foundformat;
    LinkedList<LinkedList<QString> > Lines;
    QTextDocument *doc;
    QTextCursor cursor;
    int linecount;
    QPushButton but;
    QLabel *with;
    QLabel *after;
    QLabel *put;
    QPushButton but1;
    QLineEdit *line=new QLineEdit();
    QLineEdit *line1;
    QLineEdit *line2;
    QLineEdit *line3;
    QLineEdit *line4;
    QLineEdit *line5;
    QDockWidget *Replace;
    QTextBlock textblock;
    QUndoCommand undo;
    QString string;
bool eventFilter(QObject *a,QEvent *b);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();
    void openfile();
void on_but1_clicked();

    void on_Done_clicked();

    void on_Lock_clicked();
    void on_Ins_clicked();
    void on_actionFind_triggered();
    void on_but_clicked();
    void on_del_clicked();
    void on_fontComboBox_currentIndexChanged(const QString &arg1);



    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_actionReplace_triggered();


    void on_actionDelete_triggered();

    void on_actionInsert_triggered();
    
    void on_actionSave_triggered();

    void on_Italic_clicked();

    void on_actionSelect_All_triggered();

    void on_pushButton_clicked();

    void on_actionAbout_2_triggered();

    void on_actionEdit_triggered();

    void on_actionInsert_Word_triggered();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
