#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QtGui"
#include <QFileDialog>
#include <QObject>
#include <string>
#include <QChar>
#include <QLabel>
#include <QTextCursor>
#include <QMessageBox>
#include <QDockWidget>
#include <QLineEdit>
#include <QTextCursor>
#include <QTextCharFormat>

#include <linkedlist.cpp>
#include <QObject>
#include <string>
#include <QUndoCommand>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString b=ui->textEdit->toPlainText();
    QStringList a=b.split("\n");
tempformat=ui->textEdit->currentCharFormat();
QObject::connect(ui->menuEdit,SIGNAL(triggered(QAction*)),this,SLOT(MainWindow::on_action_opentriggered()));
QObject::connect(ui->menuEdit,SIGNAL(clicked()),this,SLOT(MainWindow::on_pushButton_clicked()));


QTextCursor *cursor=new QTextCursor(ui->textEdit->document());
ui->textEdit->setTextColor("Black");
ui->textEdit->setTextCursor(*cursor);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    QString s;
    s=QFileDialog::getOpenFileName(this,
            tr("Open Text"), "/home/jana", tr("Text Files (*.txt)"));
    QFile*f=new QFile(s);
    f->open(QIODevice::ReadWrite);
    QTextStream stream(f);
    QString filetext=stream.readAll();
    f->close();
    qDebug()<<filetext;
    ui->textEdit->setText(filetext);
}
void MainWindow::openfile()
{
}

void MainWindow::on_Done_clicked()
{
    msg=new QMessageBox();
    msg->setText("Warning");
    msg->setInformativeText("Are You Sure?");
    QFont *f=new QFont("Impact",-1,-1,true);
    msg->setFont(*f);
    msg->setStandardButtons(msg->Save |msg->Cancel);
    msg->exec();
}

void MainWindow::on_Lock_clicked()
{
    //LinkedList<QString> words;
    int index=0;
    LinkedList<QString> words;
    if(ui->Lock->text()=="Lock")
    {
    ui->Lock->setText("Unlock");
    ui->textEdit->setEnabled(false);
    doc=ui->textEdit->document();
    linecount=doc->lineCount();

    for(int i=1;i<=linecount;i++)
    {
        textblock=doc->findBlockByLineNumber(i);
        string=textblock.text();
        int length=string.length();
        for(int j=0;j<length;j++)
        {
            if(string.at(j)==' '){
                words.add(string.mid(index,j-1-index));
                index=j+1;

        }

    }
        Lines.add(words);
    }
    }
            else
    {
            ui->Lock->setText("Lock");
    ui->textEdit->setEnabled(true);
    }
    update();
}




void MainWindow::on_actionFind_triggered()
{
    Find=new QDockWidget();
    Find->show();
    Find->installEventFilter(this);
    //QObject::connect(Find,SIGNAL(closed()),this,SLOT(on_Find_destroyed(QObject*)));
    line->setParent(Find);
    Find->setBaseSize(300,300);
    line->setGeometry(25,25,200,20);
    line->show();
    but.setText("Find");
    but.setParent(Find);
    but.setGeometry(50,50,50,50);
    but.show();
    but.setVisible(true);
    connect(&but,SIGNAL(clicked()),this,SLOT(on_but_clicked()));

    cursor.setCharFormat(tempformat);
    //QLabel* ab=new QLabel();
    //ab->setParent(this);
    //ab->setText("Find");
    //ab->setGeometry(500,500,200,200);
    //ab->show();
    //ab->setVisible(true);
    update();
}
void MainWindow::on_but_clicked(){

    cursor.setCharFormat(tempformat);
    if(ui->Lock->text()=="Lock")
    {
      QMessageBox lockwarning;
      lockwarning.setText("WARNING");
      lockwarning.setInformativeText("You shoud lock the text first.");
      lockwarning.exec();
    }
    else{

       // ui->textEdit->find(line->text());
        QTextDocument *doc=ui->textEdit->document();

      cursor=doc->find(line->text());
      foundformat=cursor.charFormat();
      format=new QTextCharFormat();
      format->setBackground(QBrush("red"));
      //format->setFont()
      QColor color(Qt::red);
      format->setFontUnderline(true);
      format->setUnderlineColor(color);
        cursor.setCharFormat(*format);
        //Find->closeEvent();
        //cursor.insertText("wrwerewrwer");
    }

}





void MainWindow::on_fontComboBox_currentIndexChanged(const QString &arg1)
{
    format=new QTextCharFormat;
    *format=ui->textEdit->textCursor().charFormat();
    format->setFont(arg1);
    ui->textEdit->textCursor().setCharFormat(*format);
    ui->textEdit->setFont(arg1);
}



void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    format=new QTextCharFormat();
    QColor color(arg1);
    QBrush brush(color);
    format->setForeground(brush);
    //cursor.setCharFormat(*format);
    ui->textEdit->textCursor().setCharFormat(*format);
    //QFont font;
    //font=ui->fontComboBox->currentFont();
    ui->textEdit->setTextColor(arg1);
}


void MainWindow::on_actionReplace_triggered()
{
    Replace=new QDockWidget();
    Replace->setFixedSize(500,300);
    but1.setText("Replace");
    but1.setParent(Replace);
    but1.setGeometry(100,100,100,100);
    but1.show();
    connect(&but1,SIGNAL(clicked()),this,SLOT(on_but1_clicked()));
    line1=new QLineEdit(Replace);
    line2=new QLineEdit(Replace);
    line1->setGeometry(10,50,200,20);
    line2->setGeometry(250,50,200,20);
    with=new QLabel(Replace);
    //with->setParent(Replace);
    with->setText("With:");
    with->setGeometry(215,50,200,20);
    line1->show();
    with->show();
    //line1->setFocus();
    //line2->setFocus();
    line2->show();
    Replace->show();

}
void MainWindow::on_but1_clicked(){
    QTextDocument *doc=ui->textEdit->document();
    cursor=doc->find(line1->text());
    cursor.insertText(line2->text());
}
bool MainWindow::eventFilter(QObject *a,QEvent *b)
{
    if( b->type()==QEvent::Close && qobject_cast<QObject *>(Find)==a){
        tempformat=ui->textEdit->textCursor().charFormat();
        cursor.setCharFormat(foundformat);
    }
    return QObject::eventFilter(a,b);
}


void MainWindow::on_actionDelete_triggered()
{
   Delete=new QDockWidget();
   Delete->setFixedSize(250,250);
   del=new QPushButton(Delete);
   del->setGeometry(65,150,100,100);
   del->setText("Delete");
   del->show();
   line3=new QLineEdit(Delete);
   line3->setGeometry(30,120,170,20);
   line3->show();
   connect(del,SIGNAL(clicked()),this,SLOT(on_del_clicked()));
   Delete->show();

}
void MainWindow::on_del_clicked()
{
    QTextDocument *doc=ui->textEdit->document();
    cursor=doc->find(line3->text());
    qDebug()<<cursor.selectionStart();
    qDebug()<<cursor.selectionEnd();
    cursor.removeSelectedText();

}

void MainWindow::on_actionInsert_triggered()
{
   Insert=new QDockWidget();
   Insert->setFixedSize(500,300);
   Ins=new QPushButton(Insert);
   Ins->setGeometry(265,250,100,100);
   Ins->setText("Insert");
   line4=new QLineEdit(Insert);
   line5=new QLineEdit(Insert);
   after=new QLabel(Insert);
   after->setText("After:");
   put=new QLabel(Insert);
   put->setText("Put:");
   put->setGeometry(30,200,50,20);
   put->show();
   line4->setGeometry(85,200,175,20);
   after->setGeometry(265,200,50,20);
   line5->setGeometry(310,200,175,20);
   line4->show();
   put->show();
   line5->show();
   Insert->show();
   connect(Ins,SIGNAL(clicked()),this,SLOT(on_Ins_clicked()));
}
void MainWindow::on_Ins_clicked()
{
    qDebug("Insert");
    QTextDocument *doc=ui->textEdit->document();
    cursor=doc->find(line5->text());
    int pos=cursor.selectionEnd();
    cursor.setPosition(pos);
    QString s=" "+line4->text();
    cursor.insertText(s);


}

void MainWindow::on_actionSave_triggered()
{
    QString s=QFileDialog::getSaveFileName(this,"Save File","",".txt");
    QFile *F=new QFile(s);
    F->open(QIODevice::WriteOnly);
    QTextStream stream(F);
    QString *sb=new QString();
            *sb=ui->textEdit->toPlainText();
        stream<<*sb<<endl;
    F->close();
}


void MainWindow::on_Italic_clicked()
{

    if(italic==0)
    {
        italic=1;
    format=new QTextCharFormat();
    *format=ui->textEdit->textCursor().charFormat();
    format->setFontItalic(true);
    ui->textEdit->textCursor().setCharFormat(*format);
    ui->textEdit->setFontItalic(true);
    }
    else{
        italic=0;
        ui->textEdit->setFontItalic(false);
    }
}

void MainWindow::on_actionSelect_All_triggered()
{
    ui->textEdit->selectAll();
    cursor.select(QTextCursor::Document);
}

void MainWindow::on_pushButton_clicked()
{
    if(bold==0)
    {
        bold=1;
    format=new QTextCharFormat();
    *format=ui->textEdit->textCursor().charFormat();
    format->setFontWeight(QFont::Bold);
    ui->textEdit->textCursor().setCharFormat(*format);
    ui->textEdit->setFontWeight(QFont::Bold);
    }
    else{
        bold=0;
        ui->textEdit->setFontWeight(NULL);
    }
}

void MainWindow::on_actionAbout_2_triggered()
{
    msg=new QMessageBox();
    msg->setText("TextEditor V.0.1 BETA;\nBrought to you by a bunch of workless puppets. ");
    msg->setInformativeText("Thanks For Using!!!");
    msg->exec();
}

void MainWindow::on_actionEdit_triggered()
{
    undo.undo();
}

void MainWindow::on_actionInsert_Word_triggered()
{
  Lines.getnode(1)->data.add("werewrer");
  ui->textEdit->setText(Lines.stringreturn());
}
